//TODO comments
#include "main.h"

FBullCowGame Game;

int main()
{
	do
	{
		Intro();
		Game = FBullCowGame(DifficultyReq());
		PlayGame();

		EResetMode Mode = PlayAgainReq();

		switch (Mode)
		{
		case EResetMode::REPLAY:
			Game.Reset();
			break;

		case EResetMode::NEW:
			break;
		}
		//TODO ADD ascii art
	} while (PlayAgainReq() != EResetMode::EXIT);

	return 0;
}


// prints the introduction header
void Intro()
{
	std::cout << std::endl;
	std::cout << "Welcome to Bulls&Cows!\n";
	std::cout << std::endl << std::endl;
}


// game loop
void PlayGame()
{
	std::cout << "Guess the " << Game.GetCurrentWordLength() << " letters of the isogram word i chose!\n\n";

	while (Game.GetCurrentTry() <= Game.GetMaxTries() && !Game.IsWon())
	{
		FText CurrentGuess = GetGuess();

		FBullCowCount TmpCounters = Game.evaluateGuess(CurrentGuess);
		std::cout << "Bulls: " << TmpCounters.BullsCount;
		std::cout << " - Cows: " << TmpCounters.CowsCount << "\n";

		std::cout << std::endl;
	}

	SummarizeGame();
}


// gets user guess
FText GetGuess()
{
	FText TmpGuess = "";
	EUserInputState TmpStatus;

	do
	{
		std::cout << "Try " << Game.GetCurrentTry() << " of " << Game.GetMaxTries() << " - Guess! : ";
		std::getline(std::cin, TmpGuess);

		TmpStatus = Game.IsUserInputValid(TmpGuess);

		switch (TmpStatus)
		{
		case EUserInputState::NOT_ISOGRAM:
			std::cout << "Entered word is not isogram.\n";
			break;

		case EUserInputState::CONTAINS_ILLEGAL_CHAR:
			std::cout << "Please, use only lowercase letters.\n";
			break;

		case EUserInputState::WRONG_LENGTH:
			std::cout << "Please, enter a word of the same length of hidden (" << Game.GetCurrentWordLength() << ").\n";
			break;
		}

		std::cout << std::endl;
	} while (TmpStatus != EUserInputState::OK);

	return TmpGuess;
}


// asks for playing again
EResetMode PlayAgainReq()
{
	FString TmpChose = "";
	EResetMode Chose = EResetMode::EXIT;

	std::cout << std::endl;
	
	if(Game.IsWon())
		std::cout << "Play again? (y - yes, other - no)\n";
	else std::cout << "Play again? (y - yes, other - no, s - replay same)\n";

	std::getline(std::cin, TmpChose);

	if ((TmpChose[0] == 'y') || (TmpChose[0] == 'Y')) 
		Chose = EResetMode::NEW;
	else if(!Game.IsWon() && (TmpChose[0] == 's' || TmpChose[0] == 'S')) 
		Chose = EResetMode::REPLAY;
	else Chose = EResetMode::EXIT;

	return Chose;
}


// endgame summary
void SummarizeGame()
{
	if (Game.IsWon()) std::cout << "Congratulations! You won at turn " << Game.GetCurrentTry()-1 << ".";
	else std::cout << "You lost! Try again later.";

	std::cout << std::endl << std::endl;
}


// asks for difficulty
EDifficulty DifficultyReq()
{
	EDifficulty Chose = EDifficulty::VERY_EASY;
	FString TmpChose = "";

	std::cout << "Please, select difficulty: \n";
	std::cout << std::endl;

	std::cout << "v - Very easy\n";
	std::cout << "e - Easy\n";
	std::cout << "m - Medium\n";
	std::cout << "h - Hard\n";
	std::cout << "r - Really hard!\n";

	std::cout << std::endl;

	std::cout << "Choose: ";
	std::getline(std::cin, TmpChose);

	std::cout << std::endl << std::endl;

	switch (TmpChose[0])
	{
	case 'v':
	case 'V':
		return EDifficulty::VERY_EASY;

	case 'e':
	case 'E':
		return EDifficulty::EASY;

	case 'm':
	case 'M':
		return EDifficulty::MEDIUM;

	case 'h':
	case 'H':
		return EDifficulty::HARD;

	case 'r':
	case 'R':
		return EDifficulty::REALLY_HARD;
	}
}