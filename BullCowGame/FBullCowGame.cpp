#include "FBullCowGame.h"

#pragma region Constructors
//Constructors
FBullCowGame::FBullCowGame(EDifficulty Difficulty)
{
	InitWordsMap();
	InitDifficultyMap();
	srand(__rdtsc()); // WINDOWS only, use rdtsc() for linux
	CreateNewGame(Difficulty);
}
FBullCowGame::FBullCowGame() {};
#pragma endregion

void FBullCowGame::CreateNewGame(EDifficulty Difficulty)
{
	std::tuple<int32, int32, int32> DifficultyValues = GetDifficultyValues(Difficulty);

	this->bIsWon = false;
	this->MaxTries = std::get<2>(DifficultyValues);
	this->CurrentTry = 1;

	int32 MinLength = std::get<0>(DifficultyValues);
	int32 MaxLength = std::get<1>(DifficultyValues);

	int32 DefLength = MinLength + rand() % (MaxLength - MinLength + 1);
	this->CurrentWord = GetRandomWord(DefLength);

	/*std::cout << std::endl << "length: " << MinLength << " " << MaxLength << " " << DefLength;
	std::cout << "word: " << this->CurrentWord << std::endl;*/
}


void FBullCowGame::Reset()
{
	this->CurrentTry = 1;
	this->bIsWon = false;
}


void FBullCowGame::Reset(EDifficulty Difficulty)
{
	CreateNewGame(Difficulty);
}


FBullCowCount FBullCowGame::evaluateGuess(FString Guess)
{
	FBullCowCount tmpCounters = FBullCowCount();

	for(int32 i = 0; i < (int32)Guess.size(); i++)
	{
		if (Guess[i] == this->CurrentWord[i]) tmpCounters.BullsCount++;
		else
		{ 
			for (int32 j = 0; j < (int32)this->CurrentWord.size(); j++)
			{
				if (Guess[i] == CurrentWord[j]) tmpCounters.CowsCount++;
			}
		}
	}

	this->CurrentTry++;

	if (tmpCounters.BullsCount == this->GetCurrentWordLength())
		this->bIsWon = true;

	return tmpCounters;
}


#pragma region Input Checkers
EUserInputState FBullCowGame::IsUserInputValid(FString Guess) const
{
	if (!this->ContainsIllegalChar(Guess))
		return EUserInputState::CONTAINS_ILLEGAL_CHAR;
	else if (Guess.length() != this->CurrentWord.length())
		return EUserInputState::WRONG_LENGTH;
	else if (!this->IsIsogram(Guess))
		return EUserInputState::NOT_ISOGRAM;
	else return EUserInputState::OK;
}
bool FBullCowGame::ContainsIllegalChar(FString Guess) const
{
	for (int32 i = 0; i < (int32)Guess.length(); i++)
	{
		if ((int32)Guess[i] < 97 || (int32)Guess[i] > 122)
			return false;
	}

	return true;
}


bool FBullCowGame::IsIsogram(FString Word) const
{
	TMap<char, bool> TmpMap;

	for (char c : Word)
	{
		if (TmpMap[c]) return false;
		else TmpMap[c] = true;
	}

	return true; 
}

#pragma endregion

#pragma region Getters
//Getters
int32 FBullCowGame::GetMaxTries() const { return this->MaxTries; } //TODO implement difficulty
int32 FBullCowGame::GetCurrentTry() const { return this->CurrentTry; }
int32 FBullCowGame::GetCurrentWordLength() const { return this->CurrentWord.length(); }
bool FBullCowGame::IsWon() const { return this->bIsWon; }
#pragma endregion

FString FBullCowGame::GetRandomWord(int32 length)
{
	//TODO del std::cout << std::endl << std::endl << std::endl << "length in fun: " << length;
	std::vector<FString> TmpList =  WordsMap[length];
	return TmpList[rand() % TmpList.size()];
}

std::tuple<int32, int32, int32> FBullCowGame::GetDifficultyValues(EDifficulty Difficulty)
{
	return WordLengthToDifficultyMap[Difficulty];
}

void FBullCowGame::InitWordsMap()
{

	WordsMap[3] = { "mad", "man", "map", "mug", "pad",
					"pan", "pet", "pig", "pin", "rap",
					"ray", "rum", "run", "sad", "sky",
					"son", "sum", "fan", "fax", "fur",
					"lie", "jab", "jet", "ear", "eat",
					"ego", "elf", "art" };

	WordsMap[4] = { "acid", "aged", "aloe", "army", "aunt",
					"damp", "dash", "dawn", "dear", "desk",
					"dice", "diet", "dire", "dock", "sack",
					"dune", "dusk", "mace", "maid", "main",
					"male", "make", "mark", "math", "maze",
					"melt", "mesh", "mind" };

	WordsMap[5] = { "brick", "jumpy", "words", "gleam", "using",
					"eight", "fjord", "actor", "waltz", "ducks",
					"blink", "adept", "acute", "track", "truck",
					"adult", "agent", "album", "badge", "bacon",
					"banjo", "baron", "basic", "haste", "beach",
					"beard", "beast", "magic" };

	WordsMap[6] = { "backup", "baguet", "bakery", "banish", "barony",
					"basket", "beauty", "behalf", "berlin", "bishop",
					"danger", "docent", "domain", "donkey", "donuts",
					"double", "dragon", "packet", "patron", "patrol",
					"pencil", "period", "physic", "police", "radius",
					"teacup", "thanks", "throne", "guitar"};

	WordsMap[7] = { "bailout", "balcony", "bandits", "baroque", "bastion",
					"belongs", "botanic", "bowling", "bracket", "brioche",
					"glamour", "gracile", "grimace", "machine", "mailbox",
					"mahjong", "mastery", "matches", "minster", "miracle",
					"monkeys", "mudfish", "padlock", "panther", "parking",
					"pulsant" };

	WordsMap[8] = { "cabinets", "campsite", "captions", "category", "cauldron",
					"chevrons", "chivalry", "clarinet", "daughter", "dextrous",
					"document", "ladybugs", "computer", "particle", "pointers",
					"saboteur", "sandwich", "sandworm", "scenario", "marigold",
					"matchbox", "merchant", "monarchy", "mystical", "mythical",
					"rainbows", "randomly", "reaction", "readings" };

	WordsMap[9] = {	"geraniums", "gradients", "gunpowder", "gymnastic", "factories",
					"featuring", "fisherman", "fragments", "fruitcake", "birdhouse",
					"birthdays", "biography", "boyfriend", "bricolage", "dangerous",
					"dialogues", "discharge", "discovery", "duplicate", "tenacious",
					"triangles", "scoundrel", "searching" };

	WordsMap[10] = { "background", "behaviours", "benchmarks", "binoculars", "blueprints",
				     "boundaries", "decryption", "designator", "discourage", "duplicates",
					 "duplicator", "noticeably", "palindrome", "paintbrush", "pictograms",
					 "previously", "productive", "manuscript", "misfortune", "scheduling",
					 "syncopated" };
	
	WordsMap[11] = { "abolishment", "atmospheric", "backgrounds", "complainers", "countryside",
					 "dangerously", "disgraceful", "documentary", "filmography", "lumberjacks",
					 "palindromes", "personality", "playgrounds", "precautions", "predictably",
				     "republicans", "speculation", "subordinate", "switzerland", "trampolines",
				 	 "workmanship", "compatibles", "copyrighted", "crystalized", "hydroplanes",
					 "motherlands", "punchboards", "thunderclap" };

	WordsMap[12] = { "ambidextrous", "bankruptcies", "configurated", "considerably", "demographics",
					 "exclusionary", "exhaustingly", "flowcharting", "gunpowderish", "housewarming",
					 "lexicography", "misconjugate", "overhaulings", "packinghouse", "questionably",
					 "unforgivable", "unprofitable", "lycanthropes", "earthmovings", "discountable",
					 "polycentrism", "problematics" };

	WordsMap[13] = { "documentarily", "flamethrowing", "troublemaking", "subordinately",
					 "unpredictably", "unproblematic" };
	
	WordsMap[14] = { "dermatoglyphic", "undiscoverably" };
	WordsMap[15] = { "hydropneumatics", "uncopyrightable" };
	WordsMap[16] = { "uncopyrightables" };

	/*for (int32 i = 3; i < WordsMap.size(); i++)// TODO TMP
	{
		std::cout << "length: " << i << std::endl;
		for (std::string s : WordsMap[i])
		{
			if (!IsIsogram(s)) std::cout << s << " not isogram\n";
		}
	}*/
}

void FBullCowGame::InitDifficultyMap() //TODO needs tuning
{
	WordLengthToDifficultyMap[EDifficulty::VERY_EASY] = { 3, 5, 5 };
	WordLengthToDifficultyMap[EDifficulty::EASY] = { 4, 6, 8 };
	WordLengthToDifficultyMap[EDifficulty::MEDIUM] = { 5, 8, 14 };
	WordLengthToDifficultyMap[EDifficulty::HARD] = { 8, 13, 20 };
	WordLengthToDifficultyMap[EDifficulty::REALLY_HARD] = { 12, 16, 40 };
}
