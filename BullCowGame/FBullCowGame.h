#pragma once
#include <string>
#include <vector>
#include <map>
#include "intrin.h"

#include <iostream> //TODO tmp

#define TMap std::map // To adhere Unreal coding standards
using FString = std::string; // To adhere Unreal coding standards
using int32 = int; // To adhere Unreal coding standards


struct FBullCowCount
{
	int32 BullsCount = 0;
	int32 CowsCount = 0;
};


enum class EResetMode
{
	REPLAY,
	NEW,
	EXIT
};

enum class EUserInputState
{
	OK,
	NOT_ISOGRAM,
	WRONG_LENGTH,
	CONTAINS_ILLEGAL_CHAR
};


enum class EDifficulty
{
	VERY_EASY,
	EASY,
	MEDIUM,
	HARD,
	REALLY_HARD
};


class FBullCowGame
{
public:
	FBullCowGame(EDifficulty);
	FBullCowGame();

	void Reset();
	void Reset(EDifficulty);

	EUserInputState IsUserInputValid(FString) const;	
	FBullCowCount evaluateGuess(FString);

	int32 GetMaxTries() const;
	int32 GetCurrentTry() const;
	int32 GetCurrentWordLength() const;
	bool IsWon() const;

private:
	FString CurrentWord;
	int32 CurrentTry;
	int32 MaxTries;
	bool bIsWon;
	bool IsIsogram(FString) const;
	bool ContainsIllegalChar(FString) const;

	TMap<EDifficulty, std::tuple<int32, int32, int32>> WordLengthToDifficultyMap;
	TMap<int32, std::vector<std::string>> WordsMap;
	
	void CreateNewGame(EDifficulty);

	void InitWordsMap();
	void InitDifficultyMap();

	FString GetRandomWord(int32);
	std::tuple<int32, int32, int32> GetDifficultyValues(EDifficulty);
};