#pragma once
#include <iostream>
#include <string>
#include "FBullCowGame.h"

using FText = std::string;
using int32 = int;


void Intro();
void PlayGame();
FText GetGuess();
EResetMode PlayAgainReq();
EDifficulty DifficultyReq();
void SummarizeGame();